let express = require("express");
let app = express(); //router
let path = require("path");

//подключаем движок для работы с шаблонами
app.set("view engine", "hbs");

//создаем публичную директорию
app.use(express.static(path.join(__dirname,"/public")));

app.listen(3000, "localhost", function () {
    console.log("Server started on 3000 port");
});

app.get("/", function (req, res) {
    res.send("Hello");
});

// app.get("/about", function(req,res){
//     res.send("about");
// });

// app.get("/pages/contacts", function(req,res){
//     res.send("contacts");
// });

//Для статики
app.get("/pages/:name", function(req,res){
    //console.log(req.params);
    console.log(req.params.name);
    if(req.params.name=="contacts") res.send("contacts");
    else res.send("else");
    res.send("What this?");
});

//Для динамики
app.get("/dynamic/:name/:id", function (req, res) {
    //console.log(req.params);
    //console.log(req.params.name);
    if (req.params.name == "charts") {
        if (req.params.id == "1") {
            res.send("1");
        }
    }
    else res.send("else");
    res.send("What this?");
});

app.get("/users", function(req, res){
    res.render("users.hbs", {
        title: "Список пользователей",
        body: "Вот наши пользватели",
        avatar: "/img/avatar.jpg"
    });
})


